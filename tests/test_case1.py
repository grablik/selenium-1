import allure
import pytest

from helper.basic_calc import CalcHelper

CASE_CALC = [
    {'select': 'Prototype', 'first': '2', 'second': '3', 'operation': 'Subtract', 'expect': '-1'},
    {'select': 'Prototype', 'first': 'gs', 'second': 'bu', 'operation': 'Concatenate', 'expect': 'gsbu'}
]

@allure.title('Кейс 1')
@allure.severity(allure.severity_level.NORMAL)
@pytest.mark.parametrize('case', CASE_CALC)
def test_basic_calc(case, browser):
    page = CalcHelper(browser)
    with allure.step('Открытие сайта'):
        page.go_to_calc()

    with allure.step('Скролим старницу вниз'):
        page.scroll_down()

    page.set_select_option(case['select'])
    page.set_first_input(case['first'])
    page.set_second_input(case['second'])
    page.set_select_operation(case['operation'])
    page.click_calculate_button()

    answer = page.get_answer()
    assert answer == case['expect'], "Unexpected value"
