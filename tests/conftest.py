import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

@pytest.fixture(scope="session")
def browser():
    service = Service('C://selenium//msedgedriver.exe')
    driver = webdriver.Edge(service=service)
    yield driver
    driver.quit()