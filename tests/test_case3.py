import allure

from helper.random_num import RandNumHelper


@allure.title('Кейс 3')
@allure.severity(allure.severity_level.NORMAL)
def test_rand_num(browser):
    page = RandNumHelper(browser)
    with allure.step('Открытие сайта'):
        page.go_to_rnd_num()

    with allure.step('Скролим страницу вниз'):
        page.scroll_down()

    page.set_select_build('Demo')
    page.roll_button_click()
    page.set_num_guess('string')
    page.submit_button_click()

    answer = page.get_feedback()
    assert answer == 'string: Not a number!', 'Unexpected feedback'
