from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.url_calc = "https://testsheepnz.github.io/BasicCalculator.html"
        self.url_rnd_num = "https://testsheepnz.github.io/random-number.html"

    def find_element(self, locator, timeout=5):
        return WebDriverWait(self.driver, timeout=timeout).until(EC.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_element_visibility(self, locator, timeout=5):
        try:
            return WebDriverWait(self.driver, timeout=timeout).until(EC.visibility_of_element_located(locator),
                                                      message=f"Can't find elements by locator {locator}")
        except:
            return None

    def go_to_calc(self):
        return self.driver.get(self.url_calc)

    def go_to_rnd_num(self):
        return self.driver.get(self.url_rnd_num)

    def scroll_down(self):
        return self.driver.execute_script("window.scrollTo(0, 1000)")