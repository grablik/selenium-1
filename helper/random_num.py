from tkinter import Button
from helper.base_app import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


class locators:
    LOCATOR_SELECT_BUILD = (By.NAME, 'buildNumber')
    LOCATOR_ROLLDICEBTN = (By.ID, 'rollDiceButton')
    LOCATOR_NUM_GUESS = (By.NAME, 'numberGuess')
    LOCATOR_SUBMITBTN = (By.ID, 'submitButton')
    LOCATOR_FEEDBACK = (By.ID, 'feedbackLabel')


class RandNumHelper(BasePage):


    def set_select_build(self, value):

        select = Select(self.find_element(locators.LOCATOR_SELECT_BUILD))
        # select by value
        select.select_by_visible_text(value)

    def roll_button_click(self):

        btn = self.find_element(locators.LOCATOR_ROLLDICEBTN)
        btn.click()

    def set_num_guess(self, value):

        input_guess = self.find_element_visibility(locators.LOCATOR_NUM_GUESS)
        input_guess.click()
        input_guess.send_keys(value)

    def submit_button_click(self):

        btn = self.find_element(locators.LOCATOR_SUBMITBTN)
        btn.click()

    def get_feedback(self):

        return self.find_element(locators.LOCATOR_FEEDBACK).text

