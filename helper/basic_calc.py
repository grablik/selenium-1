from helper.base_app import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


class locators:
    LOCATOR_SELECT = (By.NAME, 'selectBuild')
    LOCATOR_OPERATION = (By.NAME, 'selectOperation')
    LOCATOR_FIRST_NUM = (By.NAME, 'number1')
    LOCATOR_SECOND_NUM = (By.NAME, 'number2')
    LOCATOR_CALCULATE = (By.ID, 'calculateButton')
    LOCATOR_ANSWER = (By.NAME, 'numberAnswer')


class CalcHelper(BasePage):


    def set_select_option(self, value):

        select = Select(self.find_element(locators.LOCATOR_SELECT))
        # select by value
        select.select_by_visible_text(value)

    def set_select_operation(self, value):

        select = Select(self.find_element(locators.LOCATOR_OPERATION))
        # select by value
        select.select_by_visible_text(value)

    def set_first_input(self, value):

        input_field = self.find_element(locators.LOCATOR_FIRST_NUM)
        input_field.click()
        input_field.send_keys(value)

    def set_second_input(self, value):

        input_field = self.find_element(locators.LOCATOR_SECOND_NUM)
        input_field.click()
        input_field.send_keys(value)

    def click_calculate_button(self):

        button = self.find_element(locators.LOCATOR_CALCULATE)
        button.click()

    def get_answer(self):

        element = self.find_element(locators.LOCATOR_ANSWER)
        return element.get_attribute("value")